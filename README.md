# gwcloud_react_host_helm

Helm chart repository for gwcloud_react_host

## Assumptions
- Helm3 is [installed](https://helm.sh/docs/intro/install/)
- Relevant docker images and container image registry exists
- Relevant vault access and secrets exists
- Relevant storage access and storage solutions exist

## Repo Initialisation
- Initialise helm repo by executing `helm create gwcloud_react_host`
- Move helm output by `mv gwcloud_react_host/* . && mv gwcloud_react_host/.helmignore .`
- Cleanup `rmdir gwcloud_react_host`

## Chart values
Dynamic variables declared and initialised in [values.yaml](./values.yaml)
| Variable | Default | Comment |
| --- | --- | --- |
| `image.repository` | `nexus.gwdc.org.au/docker/gwcloud_react_host` | Container image repository |
| `image.tag` | `""` |  Main chart application `gwcloud_react_host` image tag controlled through `appVersion` in [Chart.yaml](./Chart.yaml) |
|||

## Architecture
TBA

## Prerequisites
TBA

## Support
TBA

## ToDo
- [x] Proof of concept deployment.
- [x] CI for testing the helm chart.
- [ ] CD for deploying packaged charts to `https://nexus.gwdc.org.au/#browse/browse:helm`
- [ ] CD Runtime test to dev k8s cluster.
